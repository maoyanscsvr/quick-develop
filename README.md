# 工程简介

## 项目介绍
**项目地址**: http://www.codeman.ink:8848/home
**现在主要在Github更新了哦！！！**： https://github.com/ssrskl/QuickDevelop

QuickDevelop 是一个论坛的快速开发框架,本来想做一个类似若依的管理系统的快速开发框架的,最后还是选择了论坛快速开发框架,后续会持续推出各种框架,至于里面文件前缀为dq,是因为我一开始写的时候写反了,后面懒得改了,嘻嘻嘻!

## 进度
后端基本已经完成了,除了点赞的持久化方面和管理层面,这个后续会添加的.
## 主要功能

### 1. 用户的注册与登陆
登陆功能完善了跨域问题，采用拦截器解决了跨域问题，并不再使用传统的cookie，而是采用token的形式，可以在任何客户端实现保持登陆状态
详情请见[无cookie的方法](https://swamp-nitrogen-718.notion.site/cookie-cdd1ed572208401799d4c997c4bc3a37)


### 2. 全局异常处理
采用@RestControllerAdvice和@ExceptionHandler注解捕获异常，并对异常进行处理。

### 3. 面对切面编程

当当当！当然是最典型的面对AOP编程啦，上一刀，下一刀把Controller切开，主要是用来日志输出的。

### 4. 权限控制

没有采用spring官方的SpringSecurity,而是采用了sa-token。
完善了权限的架构。完善了权限的分级

### 5. 基本功能
由于，通用查询方法，类似若依的查询方法重复代码量比较大，虽然可以通过编写代码生成器来解决。
但是我还是自己重写了QueryWrapper，采用反射和注解的方式来实现了同样的功能。
只需要在实体类中添加相应的注解，并且编写查询规则，就可以很方便的实现通用查询。
详情见：[自定义的QueryWrapper](https://blog.csdn.net/iohappyoi/article/details/118768751)

### 6. 关于前端
已经基本完成了,[CodeMan](https://gitee.com/PengGeee) 这位大佬帮我制作的,十分感谢.
我也自己制作了一个, 还没有完成, 只实现了注册登陆, 查看文章的功能,地址:[前端](https://gitee.com/maoyanscsvr/quick-develop-font)
admin管理界面还没有开始做。

### 7. 日志管理
仿造若依的日志管理,通过自定义注解,使用AOP切入,获得方法的信息等,存入数据库

### 8. 后续添加
后续添加nacos 的远程config。
添加Sentinel的限流熔断降级。

### 9. Start趋势
[![Giteye chart](https://chart.giteye.net/gitee/maoyanscsvr/quick-develop/2Z5HA39H.png)](https://giteye.net/chart/2Z5HA39H)

```
├─ sql
│ 	└─ SQL.sql                                                                      //数据库建表语句
└─ src                                                                              //源码目录
	├─ main
	│ 	├─ java
	│ 	│ 	├─ com
	│ 	│ 	│ 	└─ maoyan
	│ 	│ 	│ 		└─ quickdevelop
	│ 	│ 	│ 			├─ admin                                                     //主模块
	│ 	│ 	│ 			│ 	└─ controller
	│ 	│ 	│ 			│ 		├─ monitor
	│ 	│ 	│ 			│ 		│ 	└─ ServerController.java                         //服务器检测控制类
	│ 	│ 	│ 			│ 		├─ system                                            //系统控制器目录
	│ 	│ 	│ 			│ 		│ 	├─ dqarticle                    
	│ 	│ 	│ 			│ 		│ 	│ 	└─ DqArticleController.java                  //文章控制器
	│ 	│ 	│ 			│ 		│ 	├─ dqcomment
	│ 	│ 	│ 			│ 		│ 	│ 	└─ DqCommentController.java                  //评论控制器
	│ 	│ 	│ 			│ 		│ 	├─ DqLoginController.java                        //用户登录控制器
	│ 	│ 	│ 			│ 		│ 	├─ DqRegisterController.java                     //用户注册控制器
	│ 	│ 	│ 			│ 		│ 	├─ dqtype
	│ 	│ 	│ 			│ 		│ 	│ 	└─ DqTypeController.java                     //文章类型控制器
	│ 	│ 	│ 			│ 		│ 	├─ dquser
	│ 	│ 	│ 			│ 		│ 	│ 	└─ DqUserController.java                     //用户控制器
	│ 	│ 	│ 			│ 		│ 	└─ UploadController.java                         //文件上传控制器
	│ 	│ 	│ 			│ 		└─ TestController.java                               //测试类
	│ 	│ 	│ 			├─ common                                                    //组件目录
	│ 	│ 	│ 			│ 	├─ constant
	│ 	│ 	│ 			│ 	│ 	├─ Constants.java                                    //通用常量信息
	│ 	│ 	│ 			│ 	│ 	└─ HttpStatus.java                                   //返回的状态码
	│ 	│ 	│ 			│ 	├─ core
	│ 	│ 	│ 			│ 	│ 	├─ AjaxResult.java                                   //自定义返回格式
	│ 	│ 	│ 			│ 	│ 	├─ domain
	│ 	│ 	│ 			│ 	│ 	│ 	├─ dqabstract
	│ 	│ 	│ 			│ 	│ 	│ 	│ 	└─ DqStatusDispose.java                      //状态抽象类
	│ 	│ 	│ 			│ 	│ 	│ 	├─ DqArticle.java                                //文章实体类
	│ 	│ 	│ 			│ 	│ 	│ 	├─ DqComment.java                                //评论实体类
	│ 	│ 	│ 			│ 	│ 	│ 	├─ DqRolePermission.java                         //角色与权限对应实体类
	│ 	│ 	│ 			│ 	│ 	│ 	├─ DqType.java                                   //类型实体类
	│ 	│ 	│ 			│ 	│ 	│ 	└─ DqUser.java                                   //用户实体类
	│ 	│ 	│ 			│ 	│ 	└─ page
	│ 	│ 	│ 			│ 	│ 		└─ PageDomain.java                               //分页数据
	│ 	│ 	│ 			│ 	├─ exception
	│ 	│ 	│ 			│ 	│ 	├─ BaseException.java                                //基础异常
	│ 	│ 	│ 			│ 	│ 	├─ CustomException.java                              //自定义异常
	│ 	│ 	│ 			│ 	│ 	├─ file
	│ 	│ 	│ 			│ 	│ 	│ 	├─ FileException.java
	│ 	│ 	│ 			│ 	│ 	│ 	├─ FileNameLengthLimitExceededException.java
	│ 	│ 	│ 			│ 	│ 	│ 	├─ FileSizeLimitExceededException.java
	│ 	│ 	│ 			│ 	│ 	│ 	└─ InvalidExtensionException.java
	│ 	│ 	│ 			│ 	│ 	└─ user
	│ 	│ 	│ 			│ 	│ 		├─ CaptchaException.java
	│ 	│ 	│ 			│ 	│ 		├─ CaptchaExpireException.java
	│ 	│ 	│ 			│ 	│ 		├─ UserException.java
	│ 	│ 	│ 			│ 	│ 		└─ UserPasswordNotMatchException.java
	│ 	│ 	│ 			│ 	├─ MessageUtils.java
	│ 	│ 	│ 			│ 	├─ spring
	│ 	│ 	│ 			│ 	│ 	└─ SpringUtils.java
	│ 	│ 	│ 			│ 	├─ StringUtils.java
	│ 	│ 	│ 			│ 	└─ utils                                                   //工具类目录
	│ 	│ 	│ 			│ 		├─ Arith.java
	│ 	│ 	│ 			│ 		├─ DateUtils.java
	│ 	│ 	│ 			│ 		├─ DirectoryTreeUtil.java
	│ 	│ 	│ 			│ 		├─ DqStatusDisposrUtils.java
	│ 	│ 	│ 			│ 		├─ html
	│ 	│ 	│ 			│ 		│ 	├─ EscapeUtil.java
	│ 	│ 	│ 			│ 		│ 	└─ HTMLFilter.java
	│ 	│ 	│ 			│ 		├─ http
	│ 	│ 	│ 			│ 		│ 	├─ HttpHelper.java
	│ 	│ 	│ 			│ 		│ 	└─ HttpUtils.java
	│ 	│ 	│ 			│ 		├─ ip
	│ 	│ 	│ 			│ 		│ 	└─ IpUtils.java
	│ 	│ 	│ 			│ 		├─ MyQueryWrapper.java                                 //自定义QueryWrapper类
	│ 	│ 	│ 			│ 		├─ SaTokenUtils.java
	│ 	│ 	│ 			│ 		├─ sign
	│ 	│ 	│ 			│ 		│ 	├─ Base64.java
	│ 	│ 	│ 			│ 		│ 	└─ Md5Utils.java
	│ 	│ 	│ 			│ 		├─ text
	│ 	│ 	│ 			│ 		│ 	├─ CharsetKit.java
	│ 	│ 	│ 			│ 		│ 	├─ Convert.java
	│ 	│ 	│ 			│ 		│ 	└─ StrFormatter.java
	│ 	│ 	│ 			│ 		└─ UploadUtil.java
	│ 	│ 	│ 			├─ frameworks                                                  //框架目录
	│ 	│ 	│ 			│ 	├─ aspectj
	│ 	│ 	│ 			│ 	│ 	├─ LogAspect.java
	│ 	│ 	│ 			│ 	│ 	└─ ServiceAspect.java
	│ 	│ 	│ 			│ 	├─ config
	│ 	│ 	│ 			│ 	│ 	├─ CorsConfig.java
	│ 	│ 	│ 			│ 	│ 	├─ MybatisPlusConfig.java
	│ 	│ 	│ 			│ 	│ 	├─ SaTokenConfigure.java
	│ 	│ 	│ 			│ 	│ 	├─ SwaggerConfigure.java
	│ 	│ 	│ 			│ 	│ 	└─ UploadConfig.java
	│ 	│ 	│ 			│ 	├─ exception
	│ 	│ 	│ 			│ 	│ 	└─ GlobalExceptionHandler.java
	│ 	│ 	│ 			│ 	├─ interceptor
	│ 	│ 	│ 			│ 	└─ web
	│ 	│ 	│ 			│ 		└─ domain
	│ 	│ 	│ 			│ 			├─ server
	│ 	│ 	│ 			│ 			│ 	├─ Cpu.java
	│ 	│ 	│ 			│ 			│ 	├─ Jvm.java
	│ 	│ 	│ 			│ 			│ 	├─ Mem.java
	│ 	│ 	│ 			│ 			│ 	├─ Sys.java
	│ 	│ 	│ 			│ 			│ 	└─ SysFile.java
	│ 	│ 	│ 			│ 			└─ Server.java
	│ 	│ 	│ 			├─ QuickDevelopApplication.java
	│ 	│ 	│ 			├─ QuickDevelopApplicationRuner.java
	│ 	│ 	│ 			└─ system
	│ 	│ 	│ 				├─ domain
	│ 	│ 	│ 				│ 	├─ DqArticleVO.java
	│ 	│ 	│ 				│ 	├─ DqCommentVO.java
	│ 	│ 	│ 				│ 	├─ DqTypeVO.java
	│ 	│ 	│ 				│ 	├─ DqUserVO.java
	│ 	│ 	│ 				│ 	└─ vo
	│ 	│ 	│ 				│ 		├─ LoginVO.java
	│ 	│ 	│ 				│ 		└─ RegisterVO.java
	│ 	│ 	│ 				├─ mapper
	│ 	│ 	│ 				│ 	├─ DqArticleMapper.java
	│ 	│ 	│ 				│ 	├─ DqCommentMapper.java
	│ 	│ 	│ 				│ 	├─ DqRolePermissionMapper.java
	│ 	│ 	│ 				│ 	├─ DqTypeMapper.java
	│ 	│ 	│ 				│ 	├─ DqUserMapper.java
	│ 	│ 	│ 				│ 	└─ TestMybatis.java
	│ 	│ 	│ 				└─ service
	│ 	│ 	│ 					├─ IDqArticleService.java
	│ 	│ 	│ 					├─ IDqCommentService.java
	│ 	│ 	│ 					├─ IDqRolePermissionService.java
	│ 	│ 	│ 					├─ IDqTypeService.java
	│ 	│ 	│ 					├─ IDqUserService.java
	│ 	│ 	│ 					└─ Impl
	│ 	│ 	│ 						├─ IDqArticleServiceImpl.java
	│ 	│ 	│ 						├─ IDqCommentServiceImpl.java
	│ 	│ 	│ 						├─ IDqRolePermissionServiceImpl.java
	│ 	│ 	│ 						├─ IDqTypeServiceImpl.java
	│ 	│ 	│ 						├─ IDqUserServiceImpl.java
	│ 	│ 	│ 						└─ IStpInterfaceImpl.java
	│ 	│ 	└─ generator
	│ 	│ 		├─ domain
	│ 	│ 		│ 	└─ DqUser.java
	│ 	│ 		├─ mapper
	│ 	│ 		│ 	└─ DqUserMapper.java
	│ 	│ 		└─ service
	│ 	│ 			├─ DqUserService.java
	│ 	│ 			└─ impl
	│ 	│ 				└─ DqUserServiceImpl.java
	│ 	└─ resources
	│ 		├─ application.yml
	│ 		├─ banner.txt
	│ 		├─ i18n
	│ 		│ 	└─ messages.properties
	│ 		├─ logback.xml
	│ 		├─ mapper
	│ 		│ 	└─ DqUserMapper.xml
	│ 		├─ static
	│ 		└─ templates
	└─ test
		└─ java
			└─ com
				└─ maoyan
					└─ quickdevelop
						└─ QuickDevelopApplicationTests.java

```

